#ifndef SPARSEMATRIX_H
#define SPARSEMATRIX_H

#include <stdexcept>
#include "sparsematrixnode.h"

template <class T>
class sparseMatrix
{
public:
    typedef List<T> list;
    typedef sparseMatrixNode<T> smNode;
    list ** axisX;
    list ** axisY;

    int rows;
    int columns;
    sparseMatrix(){
        rows = 0;
        columns = 0;
    }
    sparseMatrix(int r, int c){
        rows = r;
        columns = c;
        axisX = new list*[rows];
        axisY = new list*[columns];
        for (int i = 0; i < rows; ++i)
            axisX[i] = new list(0);
        for (int i = 0; i < columns; ++i)
            axisY[i] = new list(1);
    }

    smNode operator() (int x, int y){
        if ((x > rows) || (y > columns))
            throw out_of_range("not such position");
        list* tmp[2];
        bool way = axisX[x]->getSize() < axisY[y]->getSize();
        tmp[!way] = axisX[x];
        tmp[way] = axisY[y];
        if (!way)
            swap(x, y);
        return smNode(tmp[0], tmp[1], y, x);
    }

    virtual ~sparseMatrix (){
        if (axisY && axisX){
            for (int i = 0; i < columns; ++i){
                delete axisY[i];
            }
            for (int i = 0; i < rows; ++i){
                delete axisX[i];
            }
            delete[] axisY;
            delete[] axisX;
        }
    }
};

#endif // SPARSEMATRIX_H
