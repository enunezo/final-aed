#include <iostream>
#include "list.h"
#include "sparsematrix.h"
#include <typeinfo>
#include <vector>
// #include "Operations.h"

typedef float TCELLVALUE;

using namespace std;

struct Position {
	int x;
	int y;
	Position (int _x, int _y) {
		x = _x;
		y = _y;
	}
};

struct CellMetas {
	string fName;
	vector<Position> cellsPositions;
};


// DEMO
TCELLVALUE sum (TCELLVALUE a, TCELLVALUE b) {
	return a+b;
}

class Excel {

	private:
		int width;
		int height;
		sparseMatrix<TCELLVALUE>* numbersMatrix;
		sparseMatrix<CellMetas>* metasMatrix;

		TCELLVALUE resolveMeta (string fName, vector<Position> positions ) {
			if ( !positions.size() ) return 0;
			TCELLVALUE r = (*numbersMatrix)( positions[0].x, positions[0].y );
			for ( int i=1; i<positions.size(); i++ ) {
				TCELLVALUE iValue = (*numbersMatrix)( positions[i].x, positions[i].y );
				r = sum( r, iValue );
			}
			return r;
		}

	public:
		Excel (int _width,int _height) {
			width = _width;
			height = _height;
			numbersMatrix = new sparseMatrix<TCELLVALUE>(_width, _height);
			metasMatrix = new sparseMatrix<CellMetas>(_width, _height);
		}

		virtual ~Excel() {}

		void displayMatrix () {
			for ( int y=0; y<height; y++ ) {
				for ( int x=0; x<width; x++ ) {
					cout << (*numbersMatrix)( x, y ) << "\t";
				}
				cout << endl;
			}
		}

		void setCell ( int x, int y, TCELLVALUE val, string functionName = "", vector<Position>* cells = NULL ) {
			if (functionName == "") {
				(*numbersMatrix)(x, y) = val;
				return;
			}
			(*numbersMatrix)(x, y) = resolveMeta( functionName, *cells );
		}

};