#ifndef NODE_H
#define NODE_H

#include <iostream>

using namespace std;

template <typename T>
class Node{
public:
    T data;
    Node<T> * next[2];
    int coordenates[2];
    Node(T d){
        data = d;
        next[0] = NULL;
        next[1] = NULL;
    }
};

#endif // NODE_H
