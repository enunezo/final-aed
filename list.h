#ifndef LIST_H
#define LIST_H

#include <iostream>
#include "node.h"

using namespace std;

template <typename T>
class List
{
public:
    typedef List<T> self;
    typedef Node<T> node;
    bool way;
    int size;
    T data;
    node* head;
    List(){
        way = true;
        head = NULL;
        size = 0;
    } 

    List(bool d){
        way = d;
        head = NULL;
        size = 0;
    }

    bool find (int data_, node**& p) {
        p = &head;
        while ( (*p) && ((*p)->coordenates[way] < data_))
            p = &(*p)->next[way];
        return ((*p != NULL) && ((*p)->coordenates[way] == data_));
    }

    bool insert(T data_, int otherCoord, int current, self* other){
        node** newNode;
        if (find(otherCoord, newNode)){
          (*newNode)->data = data_;
          return false;
        }
        node** op;
        other->find(current, op);
        node* tmp = (*newNode);
        (*newNode) = new node (data_);
        (*newNode)->coordenates[!way] = current;
        (*newNode)->coordenates[way] = otherCoord;
        (*newNode)->next[way] = tmp;
        tmp = (*op);
        (*newNode)->next[!way] = tmp;
        (*op) = (*newNode);
        ++(*this);
        ++(*other);
        return true;
    }

    bool remove(int otherCoord, int current, self* other){
        node** newNode;
        if (!find(otherCoord, newNode))
          return false;
        node** op;
        other->find(current, op);
        node* tmp = (*newNode);
        (*newNode) = (*newNode)->next[way];
        (*op) = (*op)->next[!way];
        delete tmp;
        --(*this);
        --(*other);
        return true;
    }

    int getSize(){
        return size;
    }

    void operator++(){
        ++size;
    }

    void operator--(){
        --size;
    }

};

#endif // LIST_H
