#ifndef SPARSEMATRIXNODE_H
#define SPARSEMATRIXNODE_H

#include <iostream>
#include "list.h"

template <class T>
class sparseMatrixNode
{
public:
    typedef List<T> list;
    typedef Node<T> node;
    typedef sparseMatrixNode<T> self;
    list*axisX;
    list*axisY;
    T zero;
    int x;
    int y;
    sparseMatrixNode(list* p1, list*p2, int x_, int y_){
        axisX = p1;
        axisY = p2;
        x= x_;
        y= y_;
    }

    operator T (){
        node** tmp;
        if (axisX->find(x, tmp))
            return (*tmp)->data;
        return (T)(NULL);
    }

    self& operator= (const T& data_){
        if (data_ == zero)
            axisX->remove(x, y, axisY);
        else
            axisX->insert(data_, x, y, axisY);
        return (*this);
    }

};

#endif // SPARSEMATRIXNODE_H
