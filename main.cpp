#include <iostream>
#include "Excel.h"

int main(){

	vector<Position> positions;
	positions.push_back( Position(1,2) );
	positions.push_back( Position(2,3) );
	positions.push_back( Position(3,4) );
	
	Excel e(10, 10);	
	e.setCell( 1, 2, 2 );
	e.setCell( 2, 3, 4 );
	e.setCell( 3, 4, 6 );
	e.setCell( 1, 1, 0, "SUM", &positions );

	e.displayMatrix();

    return 0;
}
